import arcade
from random import randint

from arcade import load_texture_pair

SCREEN_W = 1000
SCREEN_H = 650
TITLE = "тест названия"

GHOST_SCALING = 0.9
CAT_SCALING = 0.9
TILE_SCALING = 1
COIN_SCALING = 1
SPRITE_PIXEL_SIZE = 64
GRID_PIXEL_SIZE = SPRITE_PIXEL_SIZE * TILE_SCALING
PLAYER_SPEED = 5
CAT_COORDS = [[1440.0, 224.0], [1056.0, 736.0], [224.0, 736.0]]

LEFT = 0
RIGHT = 1


LEVEL = 1

class Intro(arcade.View):
    def __init__(self):
        super().__init__()
        arcade.set_viewport(0, SCREEN_W, 0, SCREEN_H)
        self.frames = []
        self.frame_num = 0
        self.cutsceneMusic = arcade.load_sound("Sounds/cutscene.wav")
        self.musicPlayer = self.cutsceneMusic.play()
        for i in range(1, 9):
            frame = arcade.Sprite(f"Игра питон/Заставки фоны/Фон_{i}.2.png", 0.5)
            frame.center_x = SCREEN_W/2
            frame.center_y = SCREEN_H/2
            self.frames.append(frame)

    def on_draw(self):
        self.clear()
        self.frames[self.frame_num].draw()

    def on_key_press(self, key, modifiers):
        if key == arcade.key.ENTER:
            self.frame_num += 1
            if self.frame_num < len(self.frames):
                self.on_draw()
            else:
                self.musicPlayer.pause()
                levelView = changeLevel()
                self.window.show_view(levelView)

class changeLevel(arcade.View):
    def __init__(self):
        super().__init__()
        arcade.set_viewport(0, SCREEN_W, 0, SCREEN_H)
        self.frame = arcade.Sprite(f"Игра питон/Заставки фоны/Уровень_{LEVEL}.png", 0.5)
        self.frame.center_x = SCREEN_W / 2
        self.frame.center_y = SCREEN_H / 2

    def on_draw(self):
        self.clear()
        self.frame.draw()

    def on_key_press(self, key, modifiers):
        if key == arcade.key.ENTER:
            gameView = MyGame()
            gameView.setup()
            self.window.show_view(gameView)

class deathScreen(arcade.View):
    def __init__(self):
        super().__init__()
        arcade.set_viewport(0, SCREEN_W, 0, SCREEN_H)
        self.frame = arcade.Sprite("Игра питон/Заставки фоны/Фон_Проигрыш.png", 0.5)
        self.frame.center_x = SCREEN_W / 2
        self.frame.center_y = SCREEN_H / 2
        self.levelMusic = arcade.load_sound("Sounds/проигрыш вам не стать котиком.wav")
        self.musicPlayer = self.levelMusic.play()

    def on_draw(self):
        self.clear()
        self.frame.draw()

    def on_key_press(self, key, modifiers):
        self.on_draw()
        if key == arcade.key.ENTER:
            self.musicPlayer.pause()
            gameView = MyGame()
            gameView.setup()
            self.window.show_view(gameView)

class finishScreen(arcade.View):
    def __init__(self):
        super().__init__()
        arcade.set_viewport(0, SCREEN_W, 0, SCREEN_H)
        self.frames = []
        self.frame_num = 0
        for i in range(1, 3):
            print(i)
            frame = arcade.Sprite(f"Игра питон/Заставки фоны/Фон_Победа{i}.png", 0.5)
            frame.center_x = SCREEN_W / 2
            frame.center_y = SCREEN_H / 2
            self.frames.append(frame)
        self.finishMusic = arcade.load_sound("Sounds/проигрыш вам не стать котиком.wav")
        self.musicPlayer = self.finishMusic.play()

    def on_draw(self):
        self.clear()
        self.frames[self.frame_num].draw()


    def on_key_press(self, key, modifiers):
        if key == arcade.key.ENTER:
            self.frame_num += 1
            if self.frame_num < len(self.frames):

                self.on_draw()
            else:
                self.musicPlayer.pause()
                global LEVEL
                LEVEL = 1
                gameView = MyGame()
                gameView.setup()
                self.window.show_view(gameView)



def load_texture_pair(filename):
    return [
        arcade.load_texture(filename),
        arcade.load_texture(filename, flipped_horizontally=True),
    ]

class PlayerCharacter(arcade.Sprite):

    def __init__(self):
        super().__init__()

        self.hasSword = False
        self.facing = 1
        self.isCat = False
        self.ghostTexturePair = load_texture_pair("Sprites/Призрак.png")
        self.ghostTexturePairWithSword = load_texture_pair("Sprites/Призрак_Меч1.png")
        self.catTexturePair = load_texture_pair("Sprites/Кот.png")
        self.catTexturePairWithSword = load_texture_pair("Sprites/Кот_Меч2.png")
        self.becomeGhost()

    def becomeGhost(self):
        self.scale = GHOST_SCALING
        self.texture = self.ghostTexturePair[self.facing]
        self.hit_box = self.texture.hit_box_points
        self.isCat = False

    def becomeCat(self):
        self.texture = self.catTexturePair[self.facing]
        self.scale = CAT_SCALING
        self.isCat = True

    def update_animation(self, delta_time: float = 1 / 60):

        if self.change_x < 0 and self.facing == RIGHT:
            self.facing = LEFT
        elif self.change_x > 0 and self.facing == LEFT:
            self.facing = RIGHT

        if self.isCat:
            self.hit_box = self.catTexturePair[self.facing].hit_box_points
        else:
            self.hit_box = self.ghostTexturePair[self.facing].hit_box_points

        if not self.isCat and not self.hasSword:
            self.texture = self.ghostTexturePair[self.facing]
        elif not self.isCat and self.hasSword:
            self.texture = self.ghostTexturePairWithSword[self.facing]
        elif self.isCat and not self.hasSword:
            self.texture = self.catTexturePair[self.facing]
        elif self.isCat and self.hasSword:
            self.texture = self.catTexturePairWithSword[self.facing]

class Monster(arcade.Sprite):
    def __init__(self):
        super().__init__()
        self.scale = 1
        self.facing = 1
        self.direction = 2 # 0 - лево 1 - право 2 - верх 3 - низ
        self.monsterTexturePair = load_texture_pair("Sprites/Монстр_64.png")

    def move(self):

        if self.direction == 0:
            self.center_x -= 2
            self.facing = RIGHT
        elif self.direction == 1:
            self.center_x += 2
            self.facing = LEFT
        elif self.direction == 2:
            self.center_y += 2
        elif self.direction == 3:
            self.center_y -= 2

    def update_animation(self, delta_time: float = 1 / 60):
        self.texture = self.monsterTexturePair[self.facing]

class MyGame(arcade.View):

    def __init__(self):
        super().__init__()
        self.tileMap = None
        self.scene = None
        self.playerSprite = None
        self.enemiesList = None
        self.physicsEngine = None
        self.camera = None
        self.gui_camera = None
        self.coinsCollected = 0
        self.coinsCount = 0
        self.finishCoords = []
        self.upgradeSound = arcade.load_sound("Sounds/UpGrade.wav")
        self.downgradeSound = arcade.load_sound("Sounds/DownGrade4.wav")
        self.looseSound = arcade.load_sound("Sounds/проигрыш вам не стать котиком.wav")
        self.takeSwordSound = arcade.load_sound("Sounds/взял меч2.wav")
        self.attackSound = arcade.load_sound("Sounds/удар мечом3.wav")
        self.collectCoinSound = arcade.load_sound("Sounds/Coin.wav")
        self.levelMusic = None
        arcade.set_background_color(arcade.csscolor.BLACK)

    def setup(self):
        self.camera = arcade.Camera(self.window.width, self.window.height)
        self.gui_camera = arcade.Camera(self.window.width, self.window.height)

        mapName = f"Levels/Level{LEVEL}.json"
#        layerOptions = {
#            "Blocks": {
#                "use_spatial_hash": True,
#            },
#            "SpawnPlayer": {
#                "use_spatial_hash": False,
#            },
#            "Floor": {
#                "use_spatial_hash": False,
#            },
#            "Sword": {
#                "use_spatial_hash": True,
#            },
#            "Coins": {
#                "use_spatial_hash": True,
#            },
#            "Cat": {
#                "use_spatial_hash": True,
#            },
#            "Monster": {
#                "use_spatial_hash": False,
#            },
#        }
        self.tileMap = arcade.load_tilemap(mapName, TILE_SCALING)
        self.scene = arcade.Scene.from_tilemap(self.tileMap)

        self.levelMusic = arcade.load_sound(f"Sounds/Level{LEVEL}.wav")
        self.levelMusic = arcade.load_sound(f"Sounds/Level{LEVEL}.wav")
        self.musicPlayer = self.levelMusic.play(volume=0.1, loop=True)

        self.playerSprite = PlayerCharacter()
        self.finishCoords.clear()
        for spawn in self.scene.get_sprite_list("SpawnPlayer"):
            self.playerSprite.center_x = spawn.center_x
            self.playerSprite.center_y = spawn.center_y
            self.finishCoords.append(spawn.center_x)
            self.finishCoords.append(spawn.center_y)
        self.scene.get_sprite_list("SpawnPlayer").clear()
        self.scene.add_sprite("Player", self.playerSprite)
        self.playerSpeed = PLAYER_SPEED

        monsterList = []
        for enemie in self.scene.get_sprite_list("Monster"):
            monster = Monster()
            monster.center_x = enemie.center_x
            monster.center_y = enemie.center_y
            monsterList.append(monster)
        self.scene.get_sprite_list("Monster").clear()

        for monster in monsterList:
            self.scene.add_sprite("Monster", monster)

        CAT_COORDS.clear()
        for catSpawnPoint in self.scene.get_sprite_list("Cat"):
            spawnPoint = []
            spawnPoint.append(catSpawnPoint.center_x)
            spawnPoint.append(catSpawnPoint.center_y)
            CAT_COORDS.append(spawnPoint)
        self.scene.get_sprite_list("Cat").clear()
        self.catRespawn()

        self.coinsCount = 0
        self.coinsCollected = 0
        for coin in self.scene.get_sprite_list("Coins"):
            self.coinsCount+=1

        self.physicsEngine = arcade.PhysicsEngineSimple(
            self.playerSprite, walls=self.scene["Blocks"],
        )

    def on_draw(self):
        self.clear()
        self.camera.use()
        self.scene.draw()
        self.gui_camera.use()
        score_text = f"Coins: {self.coinsCollected}/{self.coinsCount}"
        coinCounter = arcade.Sprite("Sprites/Монета_Малая.png")
        coinCounter.center_x = SCREEN_W - 150
        coinCounter.center_y = SCREEN_H - 17
        coinCounter.draw()
        arcade.draw_text(
            score_text,
            SCREEN_W - 125,
            SCREEN_H - 25,
            arcade.csscolor.WHITE,
            18,
            bold=True,
        )

        #self.playerSprite.draw_hit_box(arcade.color.RED, 3)

    def on_key_press(self, key, modifiers):
        if key == arcade.key.UP or key == arcade.key.W:
            self.playerSprite.change_y = self.playerSpeed
        if key == arcade.key.DOWN or key == arcade.key.S:
            self.playerSprite.change_y = -self.playerSpeed
        if key == arcade.key.LEFT or key == arcade.key.A:
            self.playerSprite.change_x = -self.playerSpeed
        if key == arcade.key.RIGHT or key == arcade.key.D:
            self.playerSprite.change_x = self.playerSpeed
        if key == arcade.key.R:
            self.musicPlayer.pause()
            self.setup()
        if key == arcade.key.T:
            self.checkWin(True)

    def on_key_release(self, key, modifiers):
        if key == arcade.key.UP or key == arcade.key.W:
            self.playerSprite.change_y = 0
        if key == arcade.key.DOWN or key == arcade.key.S:
            self.playerSprite.change_y = 0
        if key == arcade.key.LEFT or key == arcade.key.A:
            self.playerSprite.change_x = 0
        if key == arcade.key.RIGHT or key == arcade.key.D:
            self.playerSprite.change_x = 0

    def center_camera_to_player(self):
        screen_center_x = self.playerSprite.center_x - (self.camera.viewport_width/2)
        screen_center_y = self.playerSprite.center_y - (self.camera.viewport_height/2)

        horizontalBorder = self.tileMap.width*SPRITE_PIXEL_SIZE-self.camera.viewport_width
        verticalBorder = self.tileMap.height*SPRITE_PIXEL_SIZE-self.camera.viewport_height

        if screen_center_x < 0:
            screen_center_x = 0
        elif screen_center_x > horizontalBorder:
            screen_center_x = horizontalBorder
        if screen_center_y < 0:
            screen_center_y = 0
        elif screen_center_y > verticalBorder:
            screen_center_y = verticalBorder
        player_centered = screen_center_x, screen_center_y

        self.camera.move_to(player_centered)

    def on_update(self, delta_time):
        self.checkWin()
        self.physicsEngine.update()

        self.scene.update_animation(delta_time, ["Player"])
        self.scene.update_animation(delta_time, ["Monster"])
        self.moveMonsters()

        coinHitList = arcade.check_for_collision_with_list(self.playerSprite, self.scene["Coins"])
        for coin in coinHitList:
            coin.remove_from_sprite_lists()
            arcade.play_sound(self.collectCoinSound)
            self.coinsCollected+=1

        catHitList = arcade.check_for_collision_with_list(self.playerSprite, self.scene["Cat"])
        for cat in catHitList:
            self.playerSprite.becomeCat()
            cat.remove_from_sprite_lists()
            arcade.play_sound(self.upgradeSound)

        swordHitList = arcade.check_for_collision_with_list(self.playerSprite, self.scene["Sword"])
        for sword in swordHitList:
            sword.remove_from_sprite_lists()
            arcade.play_sound(self.takeSwordSound)
            self.playerSprite.hasSword = True

        monsterHitList = arcade.check_for_collision_with_list(self.playerSprite, self.scene["Monster"])
        for monster in monsterHitList:
            if self.playerSprite.hasSword:
                monster.remove_from_sprite_lists()
                arcade.play_sound(self.attackSound)
                self.playerSprite.hasSword = False
            elif self.playerSprite.isCat:
                monster.remove_from_sprite_lists()
                arcade.play_sound(self.downgradeSound)
                self.playerSprite.isCat = False
                self.catRespawn()
            else:
                self.musicPlayer.pause()
                deathScreenView = deathScreen()
                self.window.show_view(deathScreenView)

        self.center_camera_to_player()
        # GAME LOGIC !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    def checkWin(self, skipLevel = False):

        if self.coinsCollected == self.coinsCount and self.playerSprite.isCat \
        and abs(self.playerSprite.center_x - self.finishCoords[0]) < 64\
        and abs(self.playerSprite.center_y - self.finishCoords[1]) < 64\
        or skipLevel:
            self.musicPlayer.pause()
            global LEVEL
            LEVEL += 1
            if LEVEL<4:
                changeLevelScene = changeLevel()
                self.window.show_view(changeLevelScene)
            else:
                finishView = finishScreen()
                self.window.show_view(finishView)

    def catRespawn(self):
        cat = arcade.Sprite("Sprites/Кот.png")
        coords = randint(0, 2)
        cat.center_x = CAT_COORDS[coords][0]
        cat.center_y = CAT_COORDS[coords][1]
        self.scene.add_sprite("Cat", cat)

    def moveMonsters(self):
        for monster in self.scene["Monster"]:
            if (monster.center_x - 32) % 64 == 0 and (monster.center_y - 32) % 64 == 0:

                if monster.direction == 0:
                    for wall in self.scene["Blocks"]:
                        if wall.center_y == monster.center_y and (monster.center_x - wall.center_x) == 64:
                            monster.direction = randint(2, 3)
                            return
                elif monster.direction == 1:
                    for wall in self.scene["Blocks"]:
                        if wall.center_y == monster.center_y and (wall.center_x - monster.center_x) == 64:
                            monster.direction = randint(2, 3)
                            return
                elif monster.direction == 2:
                    for wall in self.scene["Blocks"]:
                        if wall.center_x == monster.center_x and (wall.center_y - monster.center_y) == 64:
                            monster.direction = randint(0, 1)
                            return
                elif monster.direction == 3:
                    for wall in self.scene["Blocks"]:
                        if wall.center_x == monster.center_x and (monster.center_y - wall.center_y) == 64:
                            monster.direction = randint(0, 1)
                            return
            monster.move()
    # GAME LOGIC END!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def main():
    window = arcade.Window(SCREEN_W, SCREEN_H, TITLE)
    startView = Intro()
    window.show_view(startView)
    #gameView = MyGame()
    #gameView.setup()
    #window.show_view(gameView)
    arcade.run()

if __name__ == "__main__":
    main()

